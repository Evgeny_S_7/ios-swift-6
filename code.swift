// п.1 - 2
class Building {
    var length : Double
    var width : Double
    var height : Double
    
    init(length: Double, width: Double, height: Double) {
        self.length = length
        self.width = width
        self.height = height
    }
    
    func create() {
        print("area: \(width * length)")
    }
    
    func destroy() {
        print("building destroyed...")
    }
}

class House: Building{
    var rooms = 0
    
    func setRooms(rooms: Int) {
        self.rooms = rooms
    }
    
    func getRooms() -> Int {
        return rooms
    }
}

class Factory: Building{
    var pipes = 0
    
    func setPipes(pipes: Int) {
        self.pipes = pipes
    }
    
    func getPipes() -> Int {
        return pipes
    }
}

var house = House(length: 6.5, width: 5.5, height: 9)
house.setRooms(rooms: 2)
print(house.getRooms())


// п.3
class Student{
    var course : Int
    var name : String
    
    init(name: String, course : Int = 1) {
        self.name = name
        self.course = course
    }
    
    func studentSort(arrStudents: inout [Student], param: Int = 0, direction: Bool = true){
        if param == 0 {
            if direction {
                return arrStudents.sort(by: { $0.course < $1.course })
            } else {
                return arrStudents.sort(by: { $0.course > $1.course })
            }
        } else if param == 1 {
            if direction {
                return arrStudents.sort(by: { $0.name < $1.name })
            } else {
                return arrStudents.sort(by: { $0.name > $1.name })
            }
        }
    }
}
class StudentGroup{
    var students : [Student]
    
    init(students: [Student]) {
        self.students = students
    }
    
    func studentSort(param: Int = 0, direction: Bool = true){
        if param == 0 {
            if direction {
                return students.sort(by: { $0.course < $1.course })
            } else {
                return students.sort(by: { $0.course > $1.course })
            }
        } else if param == 1 {
            if direction {
                return students.sort(by: { $0.name.count < $1.name.count })
            } else {
                return students.sort(by: { $0.name.count > $1.name.count })
            }
        }
    }
}

var stGroup = StudentGroup(students: [
    Student(name: "Steve"), 
    Student(name: "Saimon", course: 4), 
    Student(name: "Julie"), 
    Student(name: "Nguen", course: 2)
])

stGroup.studentSort(direction: false)

for student in stGroup.students {
    print("\(student.name): \(student.course)")
}
print()

// п.4 - отличие класса от структуры заключается в способе хранения и передачи. 
// Класс является reference type, то есть при копировании в переменную копируется только ссылка на объект,
// в то время как при копировании структуру в памяти дублируется объект структуры и по сути
// представляет новый объект, который является копией предыдущего

// п.5-8
enum CardColor : String {
    case Heart = "Heart"
    case Spades = "Spades"
    case Clubs = "Clubs"
    case Diamond = "Diamond"
}

class Card {
    var color : CardColor
    var value : Int // от 0 до 12, где 0 - карта с номиналом два; 9, 10, 11, 12 - Валет, Дама, Король, Туз
    
    init(color: CardColor, value: Int) {
        self.color = color
        self.value = value
    }
}

func getCardRandSet(amount: Int = 5) -> [Card] {
    var resCardSet = [Card]()
    var cardIndexes = [Int]()
    for var i in (0..<amount) {
        cardIndexes.append(Int.random(in: 0...51))
        i += 1
    }
    
    for cardIndex in cardIndexes {
        let carsColorIndex = cardIndex / 13 // при делении Int на Int будет целое число
        var cardColor = CardColor.Heart
        switch carsColorIndex {
            case 0:
                cardColor = CardColor.Heart
            case 1:
                cardColor = CardColor.Spades
            case 2:
                cardColor = CardColor.Clubs
            case 3:
                cardColor = CardColor.Diamond
            default:
                cardColor = CardColor.Heart
        }
        let CardValue = cardIndex % 13 // тк без остатка - карта двойка
        resCardSet.append(Card(color: cardColor, value: CardValue))
    }
    return resCardSet
}

func findPokerCombination(arrCard: inout [Card]) -> [Card] {

    func getCombination(twoPair : Bool = false) -> [Card] {
        var resCombination = [Card]()
        for card in arrCard {
            if twoPair {
                if card.value == valueCombinations[0] || card.value == valueCombinations[1] {
                    resCombination.append(card)
                }
            } else {
                if card.value == valueCombinations[0] {
                    resCombination.append(card)
                }
            }
        }
        return resCombination
    }
    
    var straight = true
    var flush = true
    arrCard.sort(by: { $0.value < $1.value })
    
    var cardValuesAmount = Array(repeating: 0, count: 13) // массив кол-ва каждого номинала
    
    let firstCardColor = arrCard[0].color
    var prevCardValue = arrCard[0].value
    for card in arrCard {
        if card.color != firstCardColor {
            flush = false
        }
        
        if card.value != prevCardValue + 1 {
            straight = false
        }
        cardValuesAmount[card.value] += 1
        prevCardValue = card.value
    }
    var valueCombinations = [Int]()
    for (value, amount) in cardValuesAmount.enumerated() {
        if amount == 2 || amount == 3 || amount == 4 {
            valueCombinations.append(value)
        }
    }
    cardValuesAmount.sort(by: { $0 > $1 })
    
    print("You have a:")
    if straight && flush && prevCardValue == 12 { //флеш рояль
        print("\(firstCardColor.rawValue) royal flush")
        return arrCard
    } else if straight && flush { // стрит флеш
        print("\(firstCardColor.rawValue) straight flush")
        return arrCard
    } else if cardValuesAmount[0] == 4 { // каре
        print("Square")
        return getCombination()
    } else if cardValuesAmount[0] == 3 && cardValuesAmount[1] == 2 { //фулл хаус
        print("Full house")
        return arrCard
    } else if flush { // флеш
        print("\(firstCardColor.rawValue) flush")
        return arrCard
    } else if straight { // стрит
        print("Straight")
        return arrCard
    } else if cardValuesAmount[0] == 3 { // тройка
        print("Triple")
        return getCombination()
    } else if cardValuesAmount[0] == 2 && cardValuesAmount[1] == 2 { // две пары
        print("Two pairs")
        return getCombination(twoPair: true)
    } else if cardValuesAmount[0] == 2 { // пара
        print("Pair")
        return getCombination()
    } else {
        var value = ""
        let highCardIndex = arrCard.count - 1
        switch arrCard[highCardIndex].value {
            case 9:
                value = "Jack"
            case 10:
                value = "Queen"
            case 11:
                value = "King"
            case 12:
                value = "Ace"
            default:
                value = String(arrCard[highCardIndex].value + 2)
        }
        print("High card \(value) of \(arrCard[highCardIndex].color.rawValue)" )
    }
    return [Card]()
}

var cardSet = getCardRandSet()

for card in cardSet {
    print("\(card.color) \(card.value)")
}
print()

let comb = findPokerCombination(arrCard: &cardSet)

print("Card in combination:")
for card in comb {
    print("\(card.color) \(card.value)")
}
print()